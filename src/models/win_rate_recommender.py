import os
import numpy as np
import requests
import json
from collections import Counter
from math import isnan

class WinRateRecommender():
    def __init__(self, verbose = True):
        self.win_against_matrix_path = f"{os.path.dirname(os.path.abspath(__file__))}/../../data/processed/win_against_matrix.csv"
        self.win_against_matrix_pos_path = f"{os.path.dirname(os.path.abspath(__file__))}/../../data/processed/win_against_matrix_pos.csv"
        self.verbose = verbose

        self.win_against_matrix = np.genfromtxt(self.win_against_matrix_path, delimiter=",")
        self.win_against_matrix_pos = np.genfromtxt(self.win_against_matrix_pos_path, delimiter=",")

        print("Making a champion translation dicts.".center(50, "="))

        x = requests.get('https://ddragon.leagueoflegends.com/cdn/10.14.1/data/en_US/champion.json')
        self.champ_keys = {}
        self.champion_translation_dict = {}
        self.champion_translation_dict[-1] = ("None", "-1")
        self.champion_translation_dict[0] = ("None", "0")
        self.champ_keys[0] = 0
        self.champ_keys_name = {}
        for i in range(len(x.json()["data"])):
            champ_name = list(x.json()["data"].keys())[i]
            champ_key = x.json()["data"][champ_name]['key']
            # my code -> (name, api_code)
            self.champion_translation_dict[i + 1] = (champ_name, champ_key)
            # api code -> my code
            self.champ_keys[int(champ_key)] = i + 1
            # name -> my code
            self.champ_keys_name[champ_name] = i + 1
        print(f"There are {len(self.champ_keys)} champions. With 0 champ for padding.")

    def test_translaction_dicts(self):
        """Test if the champion translaction dictionary works both ways
        """
        print(f"champ_key_name[Aatrox]: {self.champ_keys_name['Aatrox']}")
        print(f"champ_key_name[Diana]: {self.champ_keys_name['Diana']}")
        print(f"champion_translation_dict[1]: {self.champion_translation_dict[1]}")
        print(f"champion_translation_dict[1]: {self.champion_translation_dict[22]}")
        print(f"champ_keys[266]: {self.champ_keys[266]}")
        print(f"champ_keys[131]: {self.champ_keys[131]}")

    def print_state(self, state):
        """Print the representation of the state list

        Args:
            state (list): state list
        """
        print("The state".center(30, "="))
        bans = state[0:10]
        ally_team = state[10:15]
        ally_positions = state[15:20]
        enemy_team = state[20:25]
        
        print("Bans:")
        for ban in bans:
            print(f"{self.champion_translation_dict[ban]}",end = ", ")

        print("\nAlly team:")
        for i in range(5):
            print(f"\t{self.champion_translation_dict[ally_team[i]]}, {ally_positions[i]}")

        print("\nEnemy team:")
        for i in range(5):
            print(f"\t{self.champion_translation_dict[enemy_team[i]]}")

    def get_winrates_against(self, enemy_code, how_many = 5, threshold = 10):
        """Get best (highest winrate) champions against a given enemy

        Args:
            enemy_code (int): Enemy to check against
            how_many (int, optional): How many best options to return. Defaults to 5.
            threshold (int, optional): How many games is enough to make conclusion. Defaults to 10.

        Returns:
            list: List of the best options against a champ
        """
        out = {}
        counts = []
        for test_code in range(len(self.champ_keys)):
            wins = self.win_against_matrix[test_code, enemy_code]
            loses = self.win_against_matrix[enemy_code, test_code]
            # remove low n results
            if wins + loses < threshold:
                continue
            winrate = wins/(wins+loses)
            out[test_code] = winrate
            counts.append(wins+loses)

        # delete nan
        out = {k: out[k] for k in out if not isnan(out[k])}
        # get 3 highest values
        k = Counter(out)
        out = k.most_common(how_many)
        if self.verbose:
            print(f"Recommended vs {self.champion_translation_dict[enemy_code]} are:")
            for i in range(len(out)):
                print(f"\t{self.champion_translation_dict[out[i][0]]} with winrate {out[i][1]*100}%")
        # print(out)
        return out

    def recommend(self, state, position):
        bans = state[0:10]
        ally_team = state[10:15]
        ally_positions = state[15:20]
        enemy_team = state[20:25]
        if self.verbose:
            self.print_state(state)

        target_idx = ally_positions.index(position)
        print()
        print("Recommendations".center(30, "="))
        for i in range(len(enemy_team)):
            if enemy_team[i] != -1:
                self.get_winrates_against(enemy_team[i])


if __name__ == "__main__":
    recom = WinRateRecommender()
    test_state = [1,2,3,4,5,6,7,8,9,10,11,12,-1,-1,15,"MIDDLE","JUNGLE","BOTTOM","TOP","UTILITY",21,22,23,-1,-1]

    recom.recommend(test_state, "MIDDLE")
    # recom.test_translaction_dicts()


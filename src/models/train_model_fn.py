import wandb
import time
import sklearn.metrics
import os

import torch
# import torchvision
# import torchvision.transforms as transforms
import torch.optim as optim
# import torch.nn as nn
import torch.onnx as torch_onnx
# from torch.autograd import Variable
from src.models.icon_recognition_model import IconNet
from torch.autograd import Variable


def train(model, train_loader, test_loader, optimizer, criterion, LOG_STEP, LR, EPOCHS, LR_ADJUST_STEP, device, CURRENT_MODEL_PATH, wandb_name="lol-icon-recognition"):
    def adjust_learning_rate(optimizer, epoch, adjust_step):
        """Sets the learning rate to the initial LR decayed by 10 every adjust_step epochs"""
        lr = LR * (0.1 ** (epoch // adjust_step))
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr
        return lr

    wandb.init(project=wandb_name)
    wandb.watch(model)
    print("Training a model.".center(50, "="))

    start_time = time.time()
    lr = LR

    for epoch in range(EPOCHS):  # go through the data multiple times
        running_loss = 0.0
        val_loss = 0
        train_epoch_true = []
        train_epoch_pred = []

        val_epoch_true = []
        val_epoch_pred = []

        # adjust lr
        if epoch % 5 == 0:
            lr = adjust_learning_rate(optimizer, epoch, LR_ADJUST_STEP)

        # train
        model.train()
        for i, data in enumerate(train_loader, 0):
            # get inputs; data is (input, label)
            inputs, labels = data[0].to(device), data[1].to(device)

            # zero grads
            optimizer.zero_grad()

            # forward + backward + optim
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            train_epoch_true.extend(labels.cpu().numpy())
            outputs_softmax = torch.log_softmax(outputs, dim=1)
            _, preds = torch.max(outputs_softmax, 1)
            train_epoch_pred.extend(preds.cpu().numpy())
            # break
            running_loss += loss.item()
            # print statistics
            if i % LOG_STEP == 0 and i != 0:
                # board
                # writer.add_scalar('training loss', running_loss / LOG_STEP, epoch * len(train_loader) + i)
                wandb.log({"Train Loss": loss, "learning rate": lr})
                print(
                    f"\tEpoch: {epoch:2}\t i: {i:5}\t loss: {running_loss / LOG_STEP:.5}\t learning rate: {lr:.5}\t Training time so far: {round((time.time() - start_time))//60}m")
                running_loss = 0.0
        # val
        model.eval()
        with torch.no_grad():
            for i, data in enumerate(test_loader, 0):
                inputs, labels = data[0].to(device), data[1].to(device)
                outputs = model(inputs)
                loss = criterion(outputs, labels)
                val_loss += loss.data.item() * inputs.size(0)

                val_epoch_true.extend(labels.cpu().numpy())
                outputs_softmax = torch.log_softmax(outputs, dim=1)
                _, preds = torch.max(outputs_softmax, 1)
                val_epoch_pred.extend(preds.cpu().numpy())

        val_loss = val_loss / len(test_loader.dataset)
        print(f"Validation loss: {val_loss:.5}")
        wandb.log({"Val Loss": val_loss})

        precision, recall, fbeta, support = sklearn.metrics.precision_recall_fscore_support(
            train_epoch_true, train_epoch_pred, zero_division=0)
        acc = sklearn.metrics.accuracy_score(
            train_epoch_true, train_epoch_pred)
        # print(f"Epoch accuracy: {acc} precision: {precision}, recall: {recall}, fbeta:{fbeta}.")
        wandb.log({"Train Accuracy": acc, "Train Precision": precision})

        precision, recall, fbeta, support = sklearn.metrics.precision_recall_fscore_support(
            val_epoch_true, val_epoch_pred, zero_division=0)
        acc = sklearn.metrics.accuracy_score(
            val_epoch_true, val_epoch_pred)
        # print(f"Epoch accuracy: {acc} precision: {precision}, recall: {recall}, fbeta:{fbeta}.")
        wandb.log({"Val Accuracy": acc, "Val Precision": precision})

    print("Finished training".center(50, "="))
    print("Saving model to file.".center(50, "="))
    torch.save(model.state_dict(), CURRENT_MODEL_PATH)
    torch.save(model.state_dict(), os.path.join(wandb.run.dir, 'model.pt'))


def convert_to_onnx(n_champs, MODELS_PATH, TORCH_MODEL_PATH, ONNX_MODEL_PATH, input_shape):
    public_model_path = MODELS_PATH + "/../public/model.onnx"

    pytorch_model = IconNet(3, n_champs, is_inference=True)
    pytorch_model.load_state_dict(torch.load(TORCH_MODEL_PATH))
    pytorch_model.eval()
    # dummy_input = Variable(torch.randn(1, *input_shape))
    dummy_input = torch.zeros(28 * 28 * 3)
    output = torch_onnx.export(pytorch_model.to("cpu"),
                               dummy_input,
                               ONNX_MODEL_PATH,
                               verbose=False)

    output = torch_onnx.export(pytorch_model.to("cpu"),
                               dummy_input,
                               public_model_path,
                               verbose=False)
    print("Export to ONNX complete!")


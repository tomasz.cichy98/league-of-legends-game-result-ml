import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import torch.onnx as torch_onnx
from torch.autograd import Variable
import onnx
import json

import os
import numpy as np
import pandas as pd
import time
import matplotlib.pyplot as plt
import sklearn.metrics
import seaborn as sn
import requests

from src.models.icon_recognition_model import IconNet
from src.data.champion_translation_dict import ChampionTranslationDict
import wandb

from src.models.train_model_fn import train, convert_to_onnx

x = requests.get(
    'https://ddragon.leagueoflegends.com/api/versions.json')
GAME_VERSION = x.json()[0]
GAME_VERSION = GAME_VERSION[0:7]

ctd = ChampionTranslationDict(GAME_VERSION)        
champion_translation_dict, champ_keys, champ_keys_name = ctd.get_dicts()
N_CHAMPS = len(champ_keys_name) + 1

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"Using device: {device}")

DATA_PATH=f"{os.path.dirname(os.path.abspath(__file__))}/../../data/icons/"

# constants
TRAIN_PROC = 0.8
TEST_PROC = 0.2
EPOCHS = 10
LOG_STEP = 10
LR = 0.001
MOMENTUM = 0.9
BATCH_SIZE = 64
LR_ADJUST_STEP = 2
TRAIN = True
EVAL = False
# INPUT_SHAPE = (3,120,120)
INPUT_SHAPE = (3,28,28)

MODELS_PATH=f"{os.path.dirname(os.path.abspath(__file__))}/../../models"
CURRENT_MODEL_PATH = MODELS_PATH + f"/icon_model_ep{EPOCHS}_lr{LR}_batch{BATCH_SIZE}.pth"
ONNX_MODEL_PATH = MODELS_PATH + f"/icon_model_ep{EPOCHS}_lr{LR}_batch{BATCH_SIZE}_onnx.onnx"
OUTPUT_TRANSLATION_LIST_PUBLIC_PATH = MODELS_PATH + "/../public/translation_dict.json"


# load data from folder
transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

dataset = torchvision.datasets.ImageFolder(DATA_PATH, transform = transform)
# for js use
output_trans_json = json.dumps(dataset.classes)
with open(OUTPUT_TRANSLATION_LIST_PUBLIC_PATH, 'w') as outfile:
    json.dump(output_trans_json, outfile)

# split data
data_len = len(dataset)
train_items = round(data_len*TRAIN_PROC)
test_items = data_len - train_items
print(f"Train test: Train proc: {TRAIN_PROC}, test proc: {TEST_PROC}, train items: {train_items}, test items: {test_items}")

train_set, test_set = torch.utils.data.random_split(dataset, [train_items, test_items])

# make loaders
train_loader = torch.utils.data.DataLoader(train_set, batch_size = BATCH_SIZE, shuffle = True, num_workers = 2)
test_loader = torch.utils.data.DataLoader(test_set, batch_size = BATCH_SIZE, shuffle = False, num_workers = 2)

# make model
model = IconNet(3, N_CHAMPS).to(device)

# make loss
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=LR, momentum=MOMENTUM)

# functions to show an image
def imshow(img):
    img = img * 0.5 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()

# # get some random training images
# dataiter = iter(train_loader)
# images, labels = dataiter.next()

# # print labels
# print(' '.join('%5s' % dataset.classes[labels[j].item()] for j in range(BATCH_SIZE)))
# # show images
# imshow(torchvision.utils.make_grid(images))

if TRAIN:
    train(model, train_loader, test_loader, optimizer, criterion, LOG_STEP, LR, EPOCHS, LR_ADJUST_STEP, device, CURRENT_MODEL_PATH)

print("Loading model.".center(50, "="))
model.load_state_dict(torch.load(CURRENT_MODEL_PATH))
model.eval()

# eval
if EVAL:
    print("Starting eval.".center(50, "="))

    dataiter = iter(test_loader)
    images, labels = dataiter.next()

    print('Ground truth: ', ' '.join('%5s' % dataset.classes[labels[j].item()] for j in range(BATCH_SIZE)))
    outputs = model(images.to(device))
    outputs_softmax = torch.log_softmax(outputs, dim=1)
    _, predicted = torch.max(outputs_softmax, 1)
    predicted = predicted.cpu().numpy()
    print('Predicted: ', ' '.join('%5s' % dataset.classes[predicted[j]] for j in range(BATCH_SIZE)))
    # print images
    imshow(torchvision.utils.make_grid(images))

    print("\tGetting class acc:")
    class_correct = list(0. for i in range(N_CHAMPS))
    class_total = list(0. for i in range(N_CHAMPS))
    with torch.no_grad():
        true_list = []
        pred_list = []
        for data in test_loader:
            inputs, labels = data[0].to(device), data[1].to(device)

            labels = labels.cpu().numpy()

            outputs = model(inputs)
            outputs_softmax = torch.log_softmax(outputs, dim=1)
            _, predicted = torch.max(outputs_softmax, 1)
            predicted = predicted.cpu().numpy()
            c = (predicted == labels).squeeze()

            true_list.extend(labels)
            pred_list.extend(predicted)
            for i in range(len(labels)):
                label = labels[i]
                class_correct[label] += c[i].item()
                class_total[label] += 1

    for i in range(N_CHAMPS):
        print('Accuracy of %5s : %2d %%' % (
            dataset.classes[i], 100 * class_correct[i] / class_total[i]))

# export to onnx
convert_to_onnx(N_CHAMPS, MODELS_PATH, CURRENT_MODEL_PATH, ONNX_MODEL_PATH, INPUT_SHAPE)
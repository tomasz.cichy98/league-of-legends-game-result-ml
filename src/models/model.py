import torch
import matplotlib.pyplot as plt
import numpy as np

import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self, input_size, output_size):
        super(Net, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.fc1 = nn.Linear(self.input_size, 120)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(120, 150)
        self.fc3 = nn.Linear(150, output_size)

    def forward(self, x):
        x = self.fc1(x)
        x = self.relu(x)

        x = self.fc2(x)
        x = self.relu(x)

        x = self.fc3(x)
        # x = self.relu(x)
        # return F.softmax(x)
        return x
# net = Net(75, 148)
# print(net)
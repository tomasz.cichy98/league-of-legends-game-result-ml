import torch
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
import os
import torch
import pandas as pd
from torch.utils.data import DataLoader, random_split, Dataset
import torch.optim as optim
import torch.nn as nn
from src.models.model import Net
import requests
# from torch.utils.tensorboard import SummaryWriter
import time
import wandb
wandb.init(project="lol-predictor")

class LoLDataset(Dataset):
    def __init__(self, data_root):
        super().__init__()

        self.data = pd.read_csv(data_root)
        self.data = self.data.to_numpy()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        return self.data[idx, :-1], self.data[idx, -1]

class TrainerTester():
    def __init__(self, log_step = 100, epochs = 100, batch_size = 10, learning_rate = 0.01, force_transform_data = False, force_train = False):
        super().__init__()

        self.log_step = log_step
        self.epochs = epochs
        self.force_transform_data = force_transform_data
        self.batch_size = batch_size
        self.force_train = force_train
        self.learning_rate = learning_rate

        # define loss
        self.criterion = nn.CrossEntropyLoss()
        self.DATA_PATH=f"{os.path.dirname(os.path.abspath(__file__))}/../../data/processed/"
        self.RUNS_PATH =f"{os.path.dirname(os.path.abspath(__file__))}/runs/"
        self.MODELS_PATH=f"{os.path.dirname(os.path.abspath(__file__))}"
        self.CURRENT_MODEL_PATH = self.MODELS_PATH + f"/model_ep{self.epochs}_lr{self.learning_rate}_batch{self.batch_size}.pth"

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print(f"Using device: {self.device}")

    def make_model(self):
        print("Creating a model.".center(50, "="))
        n_out = len(self.champ_keys)
        self.model = Net(52, n_out).to(self.device)
        self.optimizer = torch.optim.SGD(self.model.parameters(), lr = self.learning_rate)
        wandb.watch(self.model)

        # # inspect the model
        # dataiter = iter(self.train_loader)
        # dat, _ = dataiter.next()
        # self.writer.add_graph(self.model, dat.float().to(self.device))

    def save_model(self):
        print("Saving model to file.".center(50, "="))
        torch.save(self.model.state_dict(), self.CURRENT_MODEL_PATH)

    def load_model(self):
        print("Loading model from a file".center(50, "="))
        # model = Net()
        if os.path.isfile(self.CURRENT_MODEL_PATH):
            self.model.load_state_dict(torch.load(self.CURRENT_MODEL_PATH))
        else:
            print("\tCouldn\'t load a model. Starting training...")
            self.train()

    def train(self):
        print("Training a model.".center(50, "="))

        # tensor board
        # self.writer = SummaryWriter(self.RUNS_PATH+"/run1")

        start_time = time.time()
        for epoch in range(self.epochs): # go through the dada multiple times
            running_loss = 0.0
            for i, data in enumerate(self.train_loader, 0):
                # get inputs; data is (input, label)
                inputs, labels = data[0].to(self.device), data[1].to(self.device)

                # zero grads
                self.optimizer.zero_grad()

                # forward + backward + optim
                outputs = self.model(inputs.float())
                loss = self.criterion(outputs, labels)
                loss.backward()
                self.optimizer.step()

                running_loss += loss.item()
                # print statistics
                if i % self.log_step == 0 and i != 0:
                    # board
                    # self.writer.add_scalar('training loss', running_loss / self.log_step, epoch * len(self.train_loader) + i)
                    wandb.log({"Train Loss": loss})
                    print(f"\tEpoch: {epoch}\t i: {i}\t loss: {running_loss / self.log_step:.5}\t Training time so far: {round((time.time() - start_time))}s")
                    running_loss = 0.0

        print("Finished training".center(50, "="))
        self.save_model()

    def eval_model(self):
        print("Evaluating a model.".center(50, "="))
        correct = 0
        total = 0
        with torch.no_grad():
            for data in self.test_loader:
                inputs, labels = data[0].to(self.device), data[1].to(self.device)
                outputs = self.model(inputs.float())
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
        print(f"Accuracy of the network is: {round(100*correct/total,2)} %")

    def get_champion_translation_dicts(self):
        print("Making a champion translation dicts.".center(50, "="))

        x = requests.get('https://ddragon.leagueoflegends.com/cdn/10.12.1/data/en_US/champion.json')
        self.champ_keys = {}
        self.champion_translation_dict = {}
        self.champ_keys[0] = 0
        for i in range(len(x.json()["data"])):
            champ_name = list(x.json()["data"].keys())[i]
            champ_key = x.json()["data"][champ_name]['key']
            self.champion_translation_dict[i] = (champ_name, champ_key)
            self.champ_keys[int(champ_key)] = i + 1

    def transform_dataset_to_new_file(self):
        print("Transforming a dataset csv file".center(50, "="))

        dataset = pd.read_csv(self.DATA_PATH + "final.csv")
        cols_to_transform = [f"pick{x}" for x in range(10)]
        cols_to_transform.extend([f"ban{x}" for x in range(10)])
        cols_to_transform.append("answer")
        # print(cols_to_transform)
        # dataset[cols_to_transform] = dataset[cols_to_transform].map(champ_keys)
        dataset[cols_to_transform] = dataset[cols_to_transform].replace(self.champ_keys)
        print(f"Dataset dims: {dataset.shape}")
        print("\tSaving to file.")
        dataset.to_csv(self.DATA_PATH + "/transformed.csv", index = False)

    def make_dataset_instance(self):
        if not os.path.isfile(self.DATA_PATH + "/transformed.csv"):
            self.transform_dataset_to_new_file()
        if self.force_transform_data:
            self.transform_dataset_to_new_file()
        # load the dataset
        self.dataset = LoLDataset(self.DATA_PATH + "/transformed.csv")
        
        self.data_train, self.data_test = train_test_split(self.dataset, test_size = 0.33, random_state=42)
        # create loaders
        self.train_loader = DataLoader(self.data_train, batch_size = 10, shuffle = True, num_workers = 2)
        self.test_loader = DataLoader(self.data_test, batch_size = 1, shuffle = False, num_workers = 2)

    def run(self):
        self.get_champion_translation_dicts()
        self.make_dataset_instance()
        self.make_model()
        if self.force_train:
            self.train()
        self.load_model()
        self.eval_model()


if __name__ == "__main__":
    tt = TrainerTester(force_train=True, force_transform_data=True)
    tt.run()

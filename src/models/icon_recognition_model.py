import torch
import numpy as np
from torchsummary import summary

import torch.nn as nn
import torch.nn.functional as F

# import torchvision
# import torchvision.transforms as transforms

class IconNet(nn.Module):
    def __init__(self, in_channels, n_classes, is_inference = False):
        """Icon recognition model with an option to be used in the inference mode for ONNX

        Args:
            in_channels (int): input image channels
            n_classes (int): number of output nodes
            is_inference (bool, optional): Include preprocessing to be used with js. Defaults to False.
        """
        # input size = (batch, 3, 28, 28)
        super(IconNet, self).__init__()
        # self.norm = transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        self.is_inference = is_inference
        self.conv1 = nn.Conv2d(in_channels, 6, 5)
        # self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 20 * 20, 300)
        self.fc2 = nn.Linear(300, 200)
        self.fc3 = nn.Linear(200, n_classes)

    def forward(self, x):
        if self.is_inference:
            x = x.reshape(1, 3, 28, 28)
            # x = self.norm(x)
            x = x/255
            x = (x - 0.5)/0.5
            # x = torch.tensor(x)

        # x = self.pool(F.relu(self.conv1(x)))
        # x = self.pool(F.relu(self.conv2(x)))
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.view(-1, 16 * 20 * 20)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        if self.is_inference:
            x = F.softmax(x, dim=1)
        return x

if __name__ == "__main__":
    model = IconNet(3, 150)
    summary(model, (3, 28, 28), device='cpu')
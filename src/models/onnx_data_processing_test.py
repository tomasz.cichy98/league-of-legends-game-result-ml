import torch
import numpy as np
import torchvision.transforms as transforms
from train_icon_model import dataset, test_set

# new input format from js
# a tensor len(3*120) with all red, then all green then all blue

# dummy_in = [x for x in range(12*12*3)]
# dummy_in = np.array(dummy_in)
dummy_in = torch.rand(12 * 12 * 3)
print(f"Dummy in:{dummy_in}")
# input shape 576,
# target shape 1, 3, 12, 12
# batch, channel, height, width
# channel[0] = [0, 1, 2, 3 ... 143]
# channel[1] = [144, 145, 146, ... 287]
# channel[2] = [288, 289, 290, ...]
print(f"Dummy in shape:{dummy_in.shape}")
# x = torch.tensor(dummy_in)
x = dummy_in
x = x.reshape(1,3,12,12)
x = torch.tensor(x)
x = x/255
x = (x-0.5)/0.5

print(f"x: {x}")
print(f"Reshaped shape:{x.shape}")
print(f"batch:0, channel:1, height:0, width: 3 = {x[0][1][0][3]}")

test_loader = torch.utils.data.DataLoader(test_set, batch_size = 1, shuffle = False, num_workers = 1)
dataiter = iter(test_loader)
images, labels = dataiter.next()
print(f"Image from the test loader: {images}")
print(f"Image denormalised: {(images*0.5)+0.5}")

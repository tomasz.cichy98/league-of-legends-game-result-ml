from riotwatcher import LolWatcher, ApiError
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import json
import seaborn as sns
import time
import requests
import os


class DataLoader:
    def __init__(
            self,
            lol_watcher,
            GAME_VERSION,
            SLEEP_TIME,
            REGIONS,
            DATA_PATH=f"{os.path.dirname(os.path.abspath(__file__))}/../../data/external/",
            force_overwrite=False,
            load_full=True,
            selected_tiers=["DIAMOND", "PLATINUM", "GOLD", "SILVER",
                            "BRONZE", "IRON", "MASTER", "GRANDMASTER", "CHALLANGER"],
            verbose=True,
            test_run=False):

        super().__init__()

        self.lol_watcher = lol_watcher
        self.data_path = DATA_PATH
        self.force_overwrite = force_overwrite
        self.SLEEP_TIME = SLEEP_TIME
        self.verbose = verbose
        self.GAME_VERSION = GAME_VERSION
        self.load_full = load_full
        self.REGIONS = REGIONS

        print(f"GAME VERSION: {GAME_VERSION}")
        print()
        print("DATA LOADER")
        print()

        # self.REGIONS = ['eun1', "br1", "euw1", "jp1",
        #                 "kr", "la1", "la2", "na1", "oc1", "tr1", "ru"]

        # self.REGIONS = ['eun1', "euw1", "jp1", "kr", "la1", "la2", "na1"]

        # test regions
        # self.REGIONS = ["jp1"]
        print(f"Using regions: {self.REGIONS}")
        if not self.load_full:
            print("Loading the smaller datset.")
            self.REGIONS = ["eun1"]
        self.QUEUES = ["RANKED_SOLO_5x5"]
        self.TIERS = ["DIAMOND", "PLATINUM",
                      "GOLD", "SILVER", "BRONZE", "IRON"]
        self.HIGH_TIERS = ["MASTER", "GRANDMASTER", "CHALLANGER"]
        self.DIVISIONS = ["I", "II", "III", "IV"]
        self.VALID_QUEUES = [400, 420, 440]
        self.SELECTED_TIERS = selected_tiers
        print(f"Using tiers: {self.SELECTED_TIERS}")

        self.players_df = pd.DataFrame()
        self.players_df_path = f"{self.data_path}players_df_{self.GAME_VERSION}_{''.join(self.REGIONS)}.csv"
        self.matches_df = pd.DataFrame()
        self.matches_df_path = f"{self.data_path}matches_tiers_df_{self.GAME_VERSION}_{''.join(self.REGIONS)}.csv"
        self.match_data_df = pd.DataFrame()
        self.match_data_df_path = f"{self.data_path}match_data_tiers_df_{self.GAME_VERSION}_{''.join(self.REGIONS)}.csv"

    def get_players_df_from_api(self):
        """ Use RiotAPI to get a list of all players in each division and region.
        """

        if self.verbose:
            # print("=="*5)
            print("Downloading players df from api...")

        for region in self.REGIONS:
            if self.verbose:
                print(f"\tProcessing region {region}.")
            for tier in self.SELECTED_TIERS:
                for division in self.DIVISIONS:
                    try:
                        players = self.lol_watcher.league.entries(
                            region=region,
                            queue=self.QUEUES[0],
                            tier=tier,
                            division=division
                        )
                        # drop last 5 cols
                        row = pd.read_json(json.dumps(players)).iloc[:, :-5]
                        # add region
                        row["region"] = region
                        # add accId
                        row["accountId"] = self.lol_watcher.summoner.by_name(
                            summoner_name=row["summonerName"][0], region=row["region"][0])["accountId"]
                        # concat
                        self.players_df = pd.concat([self.players_df, row])
                    except:
                        print("\tFailed loading a player.")
                    time.sleep(self.SLEEP_TIME)

        # save to file
        if self.verbose:
            print("Saving to file.")
        self.players_df.to_csv(self.players_df_path, index=False)

    def get_acc_id(self, row):
        """Helper to mainly to process high elo players

        Args:
            row ([type]): row of a pd dataframe

        Returns:
            str: player's encrypted accountId
        """

        try:
            out = self.lol_watcher.summoner.by_name(
                summoner_name=row["summonerName"], region=row["region"])["accountId"]
        except:
            out = np.nan

        return out

    def process_players_temp(self, players_temp, region):
        """Helper for high elo players. Processes the api response

        Args:
            players_temp (): api response for high elo league
            region ([str]): region

        Returns:
            [pd.DataFrame]: processed api response
        """

        # make a df
        players_temp_df = pd.read_json(json.dumps(
            players_temp["entries"])).iloc[:, :-4]
        players_temp_df["region"] = region
        players_temp_df["tier"] = players_temp["tier"]
        players_temp_df["leagueId"] = players_temp["leagueId"]
        players_temp_df["queueType"] = self.QUEUES[0]

        if self.verbose:
            print(f"\t\tProcessing: {players_temp_df['tier'][0]}")

        players_temp_df["accountId"] = players_temp_df.apply(
            self.get_acc_id, axis=1)

        print(f"\t\tDone: {players_temp_df['tier'][0]} \t{self.REGIONS}")
        return players_temp_df

    def process_high_elo_players_df(self):
        """Process the api response for each high elo division
        """
        if "MASTER" not in self.SELECTED_TIERS:
            print("=="*5)
            print("Skipping high elo...")
            return

        if self.verbose:
            print("=="*5)
            print("Processing high elo...")

        for region in self.REGIONS:
            print(f"\tProcessing region: {region}")

            # master players
            try:
                players_temp = self.lol_watcher.league.masters_by_queue(
                    queue=self.QUEUES[0], region=region)
            except:
                print("Failed loading a queue.")
            # concat with all players
            if len(players_temp['entries']):
                self.players_df = pd.concat(
                    [self.players_df, self.process_players_temp(players_temp, region)])

            if not self.load_full:
                print("Stopping here because test run.")
                break

            # get all grandmaster players
            try:
                players_temp = self.lol_watcher.league.grandmaster_by_queue(
                    queue=self.QUEUES[0], region=region)
            except:
                print("Failed loading a queue.")
            # concat with all players
            if len(players_temp['entries']):
                self.players_df = pd.concat(
                    [self.players_df, self.process_players_temp(players_temp, region)])

            # get all challanger players
            try:
                players_temp = self.lol_watcher.league.challenger_by_queue(
                    queue=self.QUEUES[0], region=region)
            except:
                print("Failed loading a queue.")
            # concat with all players
            if len(players_temp['entries']):
                self.players_df = pd.concat(
                    [self.players_df, self.process_players_temp(players_temp, region)])

            # print(players_df.shape)
            # drop NA
            self.players_df.dropna(inplace=True)
            self.players_df.to_csv(self.players_df_path, index=False)
        self.players_df.dropna(inplace=True)
        self.players_df.to_csv(self.players_df_path, index=False)

    def get_players_df_from_file(self):
        """Load the players_df csv file
        """
        print(f"path: {self.players_df_path}")
        if os.path.isfile(self.players_df_path):
            print("File exists. Loading from file")
            self.players_df = pd.read_csv(self.players_df_path)
        else:
            print("Failed loading from file. Using api instead.")
            self.get_players_df_from_api()

    def get_matches_df_from_api(self):
        """For each player in players_df get last 100 games. Filter by valid queues.
        """
        # just in case
        selected_tiers_df = self.players_df[self.players_df.tier.isin(
            self.SELECTED_TIERS)].reset_index(drop=True)

        start_time = time.time()

        for index, row in selected_tiers_df.iterrows():
            if index % 50 == 0 and index != 0 and self.verbose:
                print(
                    f"Done { round((index/selected_tiers_df.shape[0])*100,2) }% \t{self.REGIONS}")
                self.matches_df.to_csv(self.matches_df_path, index=False)
                if not self.load_full:
                    print("Stopping here because test run.")
                    break

            try:
                # last 100 games of a player
                matchlist = self.lol_watcher.match.matchlist_by_account(
                    row["region"], row["accountId"], end_index=100)["matches"]
                # convert to pd
                matchlist_df = pd.read_json(json.dumps(matchlist))
                # duplicate player data row
                row_multi = pd.DataFrame(row).transpose()[
                    ["summonerId", "tier"]]
                row_multi = row_multi.append(
                    [row_multi]*(matchlist_df.shape[0] - 1), ignore_index=True)
                # concat
                matchlist_df = pd.concat([row_multi, matchlist_df], axis=1)
                # concat with a games df
                self.matches_df = pd.concat([self.matches_df, matchlist_df])
            except:
                print("\tFailed loading data from api. Continuing to the next row.")
            time.sleep(self.SLEEP_TIME)

        print(f"Total time: {(time.time() - start_time) // 60} minutes.")
        # filter queues
        self.matches_df = self.matches_df[self.matches_df.queue.isin(
            self.VALID_QUEUES)].reset_index(drop=True)
        self.matches_df.to_csv(self.matches_df_path, index=False)

    def get_matches_df_from_file(self):
        """Load the matches_high_elo_df csv file
        """
        print(f"path: {self.match_data_df_path}")
        if os.path.isfile(self.matches_df_path):
            print("File exists. Loading from file")
            self.matches_df = pd.read_csv(self.matches_df_path)
            self.matches_df = self.matches_df[self.matches_df.queue.isin(
                self.VALID_QUEUES)].reset_index(drop=True)
        else:
            print("Failed loading from file. Using api instead.")
            self.get_matches_df_from_api()

    def get_match_data_df_from_api(self):
        start_time = time.time()

        game_info_keys = ["gameId", "platformId", "gameDuration",
                          "queueId", "mapId", "gameMode", "gameType"]
        game_teams_keys = ["teamId", "bans"]
        game_participant_keys = ["participantId",
                                 "teamId", "championId", "spell1Id", "spell2Id"]
        participant_stats_keys = ["win", "kills", "deaths",
                                  "assists", "totalMinionsKilled", "visionScore"]
        participant_timeline_keys = ["role", "lane", "csDiffPerMinDeltas"]

        for index, row in self.matches_df.iterrows():
            if index % 50 == 0 and index > 0:
                print(
                    f"Done {round(index/self.matches_df.shape[0]*100,2)}% \t{self.REGIONS}")
                self.match_data_df.to_csv(self.match_data_df_path, index=False)
            try:
                match_id = row["gameId"]
                game_data = self.lol_watcher.match.by_id(
                    match_id=match_id, region=row["platformId"])
                # select important data
                # game_info = pd.read_json(json.dumps(game_data["gameId", ]))

                # select game info columns
                game_info = [game_data.get(key) for key in game_info_keys]
                game_info.append(row["tier"])
                game_info = pd.DataFrame(game_info).transpose()

                # participants
                participants_info = pd.DataFrame()
                participants_stats = pd.DataFrame()
                participants_timelines = pd.DataFrame()

                for i in range(len(game_data["participants"])):
                    # participants info
                    participant_info = pd.DataFrame([game_data["participants"][i].get(
                        key) for key in game_participant_keys]).transpose()
                    participants_info = pd.concat(
                        [participants_info, participant_info], axis=0)

                    # stats
                    participant_stats = pd.DataFrame([game_data["participants"][i]["stats"].get(
                        key) for key in participant_stats_keys]).transpose()
                    participants_stats = pd.concat(
                        [participants_stats, participant_stats], axis=0)
                    # timelines
                    participant_timeline = pd.DataFrame([game_data["participants"][i]["timeline"].get(
                        key) for key in participant_timeline_keys[:-1]]).transpose()
                    # get cs diff @ 10 mint
                    cs_diff_10 = game_data["participants"][i]["timeline"].get("creepsPerMinDeltas")[
                        "0-10"]

                    participant_timeline = pd.concat(
                        [participant_timeline, pd.DataFrame([cs_diff_10])], axis=1)
                    # print(participant_timeline)

                    # participant_timeline = participant_timeline[0]
                    participants_timelines = pd.concat(
                        [participants_timelines, participant_timeline])

                # get bans
                bans = []
                # teams
                for i in range(2):
                    # bans
                    for j in range(5):
                        bans.append(game_data["teams"][i]
                                    ["bans"][j]["championId"])
                bans_df = pd.DataFrame(bans).transpose()

                # concat all
                self.match_data_df = pd.concat([self.match_data_df, pd.concat(
                    [game_info, participants_info, participants_stats, participants_timelines, bans_df], axis=1)], axis=0)
                # request limit sleep

                if index >= 500:
                    if not self.load_full:
                        print("Stopping here because test run.")
                        break
            except:
                print("Api error")
            time.sleep(self.SLEEP_TIME)

        ban_cols = [f"ban{x}" for x in range(10)]
        colnames = game_info_keys
        colnames.extend(["tier"])
        colnames.extend(game_participant_keys)
        colnames.extend(participant_stats_keys)
        colnames.extend(participant_timeline_keys)
        colnames.extend(ban_cols)
        # colnames = [game_info_keys, game_participant_keys, participant_stats_keys, participant_timeline_keys, ban_cols]
        # flatten
        # colnames = [y for x in colnames for y in x]
        # print(colnames)
        self.match_data_df.columns = colnames
        self.match_data_df.to_csv(self.match_data_df_path, index=False)

        print(f"Total time: {(time.time() - start_time) // 60} minutes.")

    def get_match_data_df_from_file(self):
        """Load the match_data_high_elo_df csv file
        """
        print(f"path: {self.match_data_df_path}")
        if os.path.isfile(self.match_data_df_path):
            print("File exists. Loading from file")
            self.match_data_df = pd.read_csv(self.match_data_df_path)
        else:
            print("Failed loading from file. Using api instead.")
            self.get_match_data_df_from_api()

    def get_players_df(self):
        """Decide how to get players df
        """
        if self.verbose:
            print("Loading players df".center(30, "="))

        if self.force_overwrite:
            self.get_players_df_from_api()
            self.process_high_elo_players_df()
        else:
            self.get_players_df_from_file()
            # self.process_high_elo_players_df()

    def get_matches_df(self):
        """Decide how to get matches df
        """
        if self.verbose:
            print("Loading matches df".center(30, "="))

        if self.force_overwrite:
            self.get_matches_df_from_api()
        else:
            self.get_matches_df_from_file()

    def get_match_data_df(self):
        """Decide how to get match data df
        """
        if self.verbose:
            print("Loading match data df".center(30, "="))

        if self.force_overwrite:
            self.get_match_data_df_from_api()
        else:
            self.get_match_data_df_from_file()

    def clean_up_matches_df(self):
        """It is possible to get 1 game multiple times from different players' perspectives.
        Remove these duplicates here.
        """
        self.matches_df.drop_duplicates(subset=["gameId"], inplace=True)
        try:
            self.matches_df = self.matches_df.drop(
                ["level_0", "index"], axis=1)
        except:
            pass
        print(f"Shape of matches_df: {self.matches_df.shape} \t{self.REGIONS}")
        # print(self.matches_df.head)
        self.matches_df.to_csv(self.matches_df_path, index=False)

    def load_data(self):
        """Main data loading function. Contains the entire pipeline
        """
        self.get_players_df()
        self.get_matches_df()
        self.clean_up_matches_df()
        self.get_match_data_df()
        print(f"Shape of match_data_df: {self.match_data_df.shape} \t{self.REGIONS}")


if __name__ == "__main__":
    API_LIMIT_1s = 20
    API_LIMIT_2m = 100
    SLEEP_TIME = 120/(API_LIMIT_2m - 1)

    # get api key
    keys_path = os.path.dirname(os.path.abspath(__file__))+"/../../keys.json"
    with open(keys_path) as f:
        keys = json.load(f)

    # get game version
    x = requests.get('https://ddragon.leagueoflegends.com/api/versions.json')
    GAME_VERSION = x.json()[0]
    GAME_VERSION_SHORT = GAME_VERSION[0:5]

    RIOT_API_KEY = keys["riot_api"]
    print(f"API key: {RIOT_API_KEY}")

    lol_watcher = LolWatcher(RIOT_API_KEY)
    dataLoader = DataLoader(lol_watcher=lol_watcher, GAME_VERSION=GAME_VERSION_SHORT,
                            SLEEP_TIME=SLEEP_TIME, force_overwrite=False, load_full=False)
    # dataLoader.get_players_df_from_api()
    dataLoader.load_data()

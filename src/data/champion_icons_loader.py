from riotwatcher import LolWatcher, ApiError
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import json
import seaborn as sns
import time
from PIL import Image
import requests
from io import BytesIO
import os
import time
import shutil

import imageio
import imgaug as ia
import imgaug.augmenters as iaa

from src.data.champion_translation_dict import ChampionTranslationDict

# http://ddragon.leagueoflegends.com/cdn/10.15.1/img/champion/Aatrox.png


class ChampionIconLoader():
    def __init__(self, DATA_PATH=f"{os.path.dirname(os.path.abspath(__file__))}/../../data/icons/"):
        self.data_path = DATA_PATH
        self.HOW_MANY_AUGMENT = 5000
        # self.IM_SIZE = 120
        self.IM_SIZE = 28

        # get game version
        x = requests.get(
            'https://ddragon.leagueoflegends.com/api/versions.json')
        self.GAME_VERSION = x.json()[0]
        self.GAME_VERSION = self.GAME_VERSION[0:7]

        self.ORIG_ICON_PATH = self.data_path + "../original_icons/"

        self.icons_link = f"http://ddragon.leagueoflegends.com/cdn/{self.GAME_VERSION}/img/champion/"
        ctd = ChampionTranslationDict(self.GAME_VERSION)
        self.champion_translation_dict, self.champ_keys, self.champ_keys_name = ctd.get_dicts()

        # image augmentation
        ia.seed(42)
        def sometimes(aug): return iaa.Sometimes(0.5, aug)
        self.seq = iaa.Sequential(
            [
                # apply the following augmenters to most images
                iaa.Fliplr(0.5),  # horizontally flip 50% of all images
                # iaa.Flipud(0.2),  # vertically flip 20% of all images
                # crop images by -5% to 10% of their height/width
                sometimes(iaa.CropAndPad(
                    percent=(-0.05, 0.1),
                    pad_mode=ia.ALL,
                    pad_cval=(0, 255)
                )),
                sometimes(iaa.Affine(
                    # scale images to 80-120% of their size, individually per axis
                    scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
                    # translate by -20 to +20 percent (per axis)
                    translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
                    rotate=(-30, 30),  # rotate by -45 to +45 degrees
                    shear=(-16, 16),  # shear by -16 to +16 degrees
                    # use nearest neighbour or bilinear interpolation (fast)
                    order=[0, 1],
                    # if mode is constant, use a cval between 0 and 255
                    cval=(0, 255),
                    # use any of scikit-image's warping modes (see 2nd image from the top for examples)
                    mode=ia.ALL
                )),
                # execute 0 to 5 of the following (less important) augmenters per image
                # don't execute all of them, as that would often be way too strong
                iaa.SomeOf((0, 5),
                           [
                    # convert images into their superpixel representation
                    # sometimes(iaa.Superpixels(
                    #     p_replace=(0, 0.5), n_segments=(20, 200))),
                    iaa.OneOf([
                        # blur images with a sigma between 0 and 3.0
                        iaa.GaussianBlur((0, 1)),
                        # blur image using local means with kernel sizes between 2 and 7
                        # iaa.AverageBlur(k=(2, 5)),
                        # blur image using local medians with kernel sizes between 2 and 7
                        # iaa.MedianBlur(k=(3, 5)),
                    ]),
                    iaa.Sharpen(alpha=(0, 1.0), lightness=(
                        0.75, 1.25)),  # sharpen images
                    # iaa.Emboss(alpha=(0, 1.0), strength=(
                    #     0, 2.0)),  # emboss images
                    # search either for all edges or for directed edges,
                    # blend the result with the original image using a blobby mask
                    # iaa.BlendAlphaSimplexNoise(iaa.OneOf([
                    #     iaa.EdgeDetect(alpha=(0.3, 0.5)),
                    #     iaa.DirectedEdgeDetect(
                    #         alpha=(0.3, 0.5), direction=(0.0, 1.0)),
                    # ])),
                    # add gaussian noise to images
                    iaa.AdditiveGaussianNoise(loc=0, scale=(
                        0.0, 0.01*255), per_channel=0.3),
                    iaa.OneOf([
                        # randomly remove up to 10% of the pixels
                        iaa.Dropout((0.01, 0.1), per_channel=0.3),
                        iaa.CoarseDropout((0.01, 0.1), size_percent=(
                            0.02, 0.05), per_channel=0.2),
                    ]),
                    # invert color channels
                    # iaa.Invert(0.05, per_channel=True),
                    # change brightness of images (by -10 to 10 of original value)
                    iaa.Add((-5, 5), per_channel=0.3),
                    # change hue and saturation
                    iaa.AddToHueAndSaturation((-5, 5)),
                    # either change the brightness of the whole image (sometimes
                    # per channel) or change the brightness of subareas
                    iaa.OneOf([
                        iaa.Multiply((0.9, 1.1), per_channel=0.3),
                        iaa.BlendAlphaFrequencyNoise(
                            exponent=(-1.1, 0),
                            foreground=iaa.Multiply(
                                (0.5, 1.2), per_channel=True),
                            background=iaa.LinearContrast((0.5, 1.2))
                        )
                    ]),
                    # improve or worsen the contrast
                    iaa.LinearContrast((0.5, 2.0), per_channel=0.5),
                    # iaa.Grayscale(alpha=(0.0, 0.5)),
                    # move pixels locally around (with random strengths)
                    sometimes(iaa.ElasticTransformation(
                        alpha=(0.5, 1.1), sigma=0.25)),
                    # sometimes move parts of the image around
                    sometimes(iaa.PiecewiseAffine(scale=(0.01, 0.02))),
                    sometimes(iaa.PerspectiveTransform(scale=(0.01, 0.1)))
                ],
                    random_order=True
                )
            ],
            random_order=True
        )

    def delete_existing_folders(self):
        """Clean up the data from the previous augmentations
        """
        for champ in self.champion_translation_dict.items():
            champ_name = champ[1][0]
            if champ_name != "None":
                folder_path = self.data_path + champ_name + "/"
                if os.path.exists(folder_path):
                    shutil.rmtree(folder_path)
        if os.path.exists(self.data_path+"None/"):
            shutil.rmtree(self.data_path+"None/")

    def augment_none_icon(self):
        """None icon can not be downloaded from api. Augment it using the version in the original_icons folder
        """
        aug_icon_path = self.data_path+"None/"
        icon = imageio.imread(self.ORIG_ICON_PATH+"None.png")
        icon = self.resize_icon(icon)
        if not os.path.exists(self.data_path+"None/"):
            os.makedirs(self.data_path+"None/")

        self.augment_icon(icon, aug_icon_path)

    def augment_icon(self, img, folder_path):
        """Generate more version of one icon and save to file

        Args:
            img (imageio): image to augment
            folder_path (str): destination folder
        """
        images = [img for x in range(self.HOW_MANY_AUGMENT)]
        images_aug = self.seq(images=images)
        for image in images_aug:
            image_path = folder_path + str(time.time()) + ".png"
            imgPIL = Image.fromarray(image)
            imgPIL.save(image_path)
            # imageio.imwrite(image_path, image)

        # ia.imshow(np.hstack(images_aug))

    def get_one_icon(self, champ_name):
        """Get an icon from the DataDragon

        Args:
            champ_name (str): Name of the champion of which the icon is downloaded
        """
        url = self.icons_link + champ_name + ".png"
        folder_path = self.data_path + champ_name + "/"
        orig_path = self.ORIG_ICON_PATH + champ_name + ".png"

        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        if not os.path.exists(self.ORIG_ICON_PATH):
            os.makedirs(self.ORIG_ICON_PATH)

        print(f"URL: {url}")
        response = requests.get(url)
        img = imageio.imread(url)
        # save original
        imageio.imwrite(orig_path, img)
        img_np = self.resize_icon(img)
        # augment
        self.augment_icon(img_np, folder_path)

    def resize_icon(self, img):
        imgPIL = Image.fromarray(img)
        imgPIL = imgPIL.resize((self.IM_SIZE, self.IM_SIZE))
        # imgPIL.show()
        img_out = np.array(imgPIL)
        return img_out

    def load_data(self):
        """Main function, run the pipeline
        """
        self.delete_existing_folders()
        self.augment_none_icon()
        for champ in self.champion_translation_dict.items():
            champ_name = champ[1][0]
            if champ_name != "None":
                self.get_one_icon(champ_name)

    def test(self):
        for champ in self.champion_translation_dict.items():
            champ_name = champ[1][0]
            if champ_name != "None":
                # self.get_one_icon(champ_name)
                url = self.icons_link + champ_name + ".png"
                response = requests.get(url)
                img = imageio.imread(url)
                img_new = Image.fromarray(img)
                print(type(img))
                print(type(img_new))
                img_small = self.resize_icon(img)
                print(type(img_small))
                break


if __name__ == "__main__":
    cil = ChampionIconLoader()
    # cil.test()
    cil.load_data()

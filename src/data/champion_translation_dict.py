import requests

class ChampionTranslationDict():
    def __init__(self, GAME_VERSION):

        self.GAME_VERSION = GAME_VERSION    
        print("Making a champion translation dicts.".center(50, "="))

        x = requests.get(f'https://ddragon.leagueoflegends.com/cdn/{self.GAME_VERSION}/data/en_US/champion.json')
        self.champ_keys = {}
        self.champion_translation_dict = {}
        self.champion_translation_dict[-1] = ("None", "-1")
        # self.champion_translation_dict[0] = ("None", "0")
        self.champ_keys[0] = 0
        self.champ_keys_name = {}
        for i in range(len(x.json()["data"])):
            champ_name = list(x.json()["data"].keys())[i]
            champ_key = x.json()["data"][champ_name]['key']
            # my code -> (name, api_code)
            self.champion_translation_dict[i] = (champ_name, champ_key)
            # api code -> my code
            self.champ_keys[int(champ_key)] = i
            # name -> my code
            self.champ_keys_name[champ_name] = i

        print(f"There are {len(self.champ_keys_name)} champions excluding the None champ.")

    def get_dicts(self):
        return self.champion_translation_dict, self.champ_keys, self.champ_keys_name

if __name__ == "__main__":
    ctd = ChampionTranslationDict("10.16.1")
    ctd.get_dicts()
    print(ctd.champ_keys_name["Leblanc"])
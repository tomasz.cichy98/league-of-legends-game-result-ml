import numpy as np
import pandas as pd
import json
from PIL import Image
import requests
from io import BytesIO
import os
import time
import shutil

import matplotlib.pyplot as plt
import matplotlib.patches as patches

import cv2

import imageio
import imgaug as ia
import imgaug.augmenters as iaa
from imgaug.augmentables.polys import Polygon, PolygonsOnImage

from champion_translation_dict import ChampionTranslationDict

class ChampSelectDatasetCreator():
    def __init__(self, data_root = f"{os.path.dirname(os.path.abspath(__file__))}/../../data/"):
        self.ORIG_ICON_FOLDER = data_root + "original_icons/"
        self.TARGET_FOLDER = data_root + "champ_select/"
        self.BASE_IMG_PATH = data_root + "champ_select.png"
        self.BASE_IMG_BOXES = data_root + "champ_select.json"
        self.BAN_ICON_SIZE = 28
        self.PICK_ICON_SIZE = 60
        
        # get game version
        x = requests.get('https://ddragon.leagueoflegends.com/api/versions.json')
        self.GAME_VERSION = x.json()[0]
        self.GAME_VERSION = self.GAME_VERSION[0:7]
        print(f"Game version: {self.GAME_VERSION}")

        ctd = ChampionTranslationDict(self.GAME_VERSION)        
        self.champion_translation_dict, self.champ_keys, self.champ_keys_name = ctd.get_dicts()
        
        # read base img
        self.base_img = imageio.imread(self.BASE_IMG_PATH)

        self.ban_boxes = {
            # (top right y, top right x)
            "ban1": (31, 16),
            "ban2": (31, 56),
            "ban3": (31, 96),
            "ban4": (31, 136),
            "ban5": (31, 176),
            "ban6": (31, 1077),
            "ban7": (31, 1117),
            "ban8": (31, 1157),
            "ban9": (31, 1197),
            "ban10": (31, 1237),            
        }
        self.pick_boxes = {
            "pick1": (104, 56),
            "pick2": (184, 56),
            "pick3": (264, 56),
            "pick4": (344, 56),
            "pick5": (424, 56),
            "pick6": (104, 1202),
            "pick7": (184, 1202),
            "pick8": (264, 1202),
            "pick9": (344, 1202),
            "pick10": (424, 1202),
        }
        
        # make polygons
        self.polygons = {}
        for box in self.ban_boxes.items():
            self.polygons[box[0]] = Polygon([
                (box[1][1], box[1][0]),
                (box[1][1] + self.BAN_ICON_SIZE, box[1][0]),
                (box[1][1] + self.BAN_ICON_SIZE, box[1][0] + self.BAN_ICON_SIZE),
                (box[1][1], box[1][0] + self.BAN_ICON_SIZE),
            ])
        for box in self.pick_boxes.items():
            self.polygons[box[0]] = Polygon([
                (box[1][1], box[1][0]),
                (box[1][1] + self.PICK_ICON_SIZE, box[1][0]),
                (box[1][1] + self.PICK_ICON_SIZE, box[1][0] + self.PICK_ICON_SIZE),
                (box[1][1], box[1][0] + self.PICK_ICON_SIZE),
            ])

        # create psoi
        self.polygons_list = []
        for key, val in self.polygons.items():
            self.polygons_list.append(val)
        self.psoi = ia.PolygonsOnImage(self.polygons_list, shape=self.base_img.shape)

        # aug seq
        ia.seed(1)
        self.seq = iaa.Sequential([
            iaa.Fliplr(0.5), # horizontal flips
            iaa.Crop(percent=(0, 0.01)), # random crops
            # Small gaussian blur with random sigma between 0 and 0.5.
            # But we only blur about 50% of all images.
            iaa.Sometimes(
                0.5,
                iaa.GaussianBlur(sigma=(0, 0.5))
            ),
            # Strengthen or weaken the contrast in each image.
            iaa.LinearContrast((0.75, 1.5)),
            # Add gaussian noise.
            # For 50% of all images, we sample the noise once per pixel.
            # For the other 50% of all images, we sample the noise per pixel AND
            # channel. This can change the color (not only brightness) of the
            # pixels.
            iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
            # Make some images brighter and some darker.
            # In 20% of all cases, we sample the multiplier once per channel,
            # which can end up changing the color of the images.
            iaa.Multiply((0.8, 1.2), per_channel=0.2),
            # Apply affine transformations to each image.
            # Scale/zoom them, translate/move them, rotate them and shear them.
            iaa.Affine(
                scale={"x": (0.8, 1), "y": (0.8, 1)},
                # translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
                rotate=(-5, 5),
                # shear=(-8, 8)
            )
        ], random_order=True) # apply augmenters in random order

    def show_polygons(self):
        """Show the base image with polygons on it
        """
        image_poly = np.copy(self.base_img)
        for poly in self.polygons.items():
            image_poly = poly[1].draw_on_image(image_poly, alpha_face=0.2, size_points=7)
        ia.imshow(image_poly)

    def show_polys_aug(self, img, polys):
        pass

    def aug_one_image(self, img, polys):
        """Augment one image

        Args:
            img (): input image
            polys (psoi): input polygons for the image
        """
        images_polys_aug = []
        for _ in range(2*4):
            image_aug, psoi_aug = self.seq(image=img, polygons=polys)
            image_polys_aug = psoi_aug.draw_on_image(image_aug, alpha_face=0.2, size_points=11)
            images_polys_aug.append(ia.imresize_single_image(image_polys_aug, 0.5))
        ia.imshow(ia.draw_grid(images_polys_aug, cols=2))

    def run(self):
        self.aug_one_image(self.base_img, self.psoi)

    def show_all_boxes(self):
        """Show the base image with the boxes on it
        """
        fig, ax = plt.subplots(1)

        ax.imshow(self.base_img)

        for box in self.ban_boxes.items():
            print(box[1])
            rect = patches.Rectangle((box[1][1], box[1][0]), self.BAN_ICON_SIZE, self.BAN_ICON_SIZE, linewidth = 1, edgecolor='r', facecolor='none')
            ax.add_patch(rect)

        for box in self.pick_boxes.items():
            print(box[1])
            rect = patches.Rectangle((box[1][1], box[1][0]), self.PICK_ICON_SIZE, self.PICK_ICON_SIZE, linewidth = 1, edgecolor='r', facecolor='none')
            ax.add_patch(rect)

        plt.show()

    def augment_image(self):
        pass


if __name__ == "__main__":
    csdc = ChampSelectDatasetCreator()
    # csdc.show_all_boxes()
    csdc.show_polygons()
    csdc.run()
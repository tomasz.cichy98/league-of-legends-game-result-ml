import numpy as np
import pandas as pd
import json
import requests
from riotwatcher import LolWatcher, ApiError
from roleidentification import pull_data, get_roles
import os
import itertools
from src.data.data_loader import DataLoader
import matplotlib.pyplot as plt

from src.data.champion_translation_dict import ChampionTranslationDict

NUMBER_OF_CHAMPIONS = 149

# subclass JSONEncoder
class setEncoder(json.JSONEncoder):
    """Allow saving set as json

    Args:
        json ([type]): [description]
    """
    def default(self, obj):
        return list(obj)

class DataAugmenter():
    def __init__(self, match_data_df, match_data_path, DATA_PATH=f"{os.path.dirname(os.path.abspath(__file__))}/../../data/processed/"):
        super().__init__()

        print()
        print("DATA AUGMENTER")
        print()


        # get game version
        x = requests.get(
            'https://ddragon.leagueoflegends.com/api/versions.json')
        self.GAME_VERSION = x.json()[0]
        self.GAME_VERSION = self.GAME_VERSION[0:7]

        self.match_data_df = pd.DataFrame(match_data_df)
        self.augmented_df = pd.DataFrame()
        self.champ_pos_dict = {
            "TOP": set(),
            "JUNGLE": set(),
            "MIDDLE": set(),
            "BOTTOM": set(),
            "UTILITY": set()
            }
        self.champ_pos_dict_path = f"{DATA_PATH}champ_pos_dict.json"

        self.match_data_df_path = match_data_path
        self.game_selected_cols = pd.DataFrame()
        self.pos_data = pull_data()
        self.data_path = DATA_PATH
        self.final_df = pd.DataFrame()       

        self.augmented_path = f"{self.data_path}match_data_augmented.csv"
        self.final_df_path = f"{self.data_path}final.csv"
        self.win_against_matrix_pos_path = f"{self.data_path}win_against_matrix_pos.csv"
        self.win_against_matrix_path = f"{self.data_path}win_against_matrix.csv"
        self.win_against_json_path = f"{self.data_path}win_against.json"
        self.win_against_pos_json_path = f"{self.data_path}win_against_pos.json"
        self.win_against_pos_json_path_public = f"{self.data_path}../../public_src/win_against_pos.json"


        self.player_ids = [x for x in range(10)]

        ctd = ChampionTranslationDict(self.GAME_VERSION)
        self.champion_translation_dict, self.champ_keys, self.champ_keys_name = ctd.get_dicts()
        self.NUMBER_OF_CHAMPIONS = len(self.champ_keys_name)

        self.win_against_matrix = -1*np.identity(self.NUMBER_OF_CHAMPIONS + 1, dtype = np.int8)
        self.win_against_matrix_pos = -1*np.identity(self.NUMBER_OF_CHAMPIONS + 1, dtype = np.int8)
        self.win_with_matrix = -1*np.identity(self.NUMBER_OF_CHAMPIONS + 1, dtype = np.int8)

    def populate_champ_pos_dict(self):
        """Populate position dict using observed positions. Use sets instead of lists
        """
        print("Populating champ pos dict.".center(50, "="))
        for index, row in self.match_data_df.iterrows():
            champ_id = int(row["championId"])
            position = str(row["position"])
            my_champ_code = self.champ_keys[champ_id]
            champ_name = self.champion_translation_dict[my_champ_code][0]
            # print(position)
            # print(f"Champ_id:{champ_id}")
            # print(f"My champ code: {my_champ_code}")
            # print(f"Champ name: {champ_name}")

            self.champ_pos_dict[position].add(champ_name)

        print("Saving champion pos dict to json file.")
        print(f"path: {self.champ_pos_dict_path}")
        with open(self.champ_pos_dict_path, 'w') as json_file:
            json.dump(self.champ_pos_dict, json_file, cls=setEncoder)
        

    def decode_champion(self, champ_code):
        return self.champion_translation_dict[champ_code]

    def get_winrate_using_names(self, name_1, name_2):
        """Return a winrate of champion 1 against champion 2.

        Args:
            name_1 (string): name of the champions
            name_2 (string): name of the opposing champion

        Returns:
            float: winrate between 0 and 1
        """
        code_1 = self.champ_keys_name[name_1]
        code_2 = self.champ_keys_name[name_2]

        wins = self.win_against_matrix[code_1, code_2]
        loses = self.win_against_matrix[code_2, code_1]

        winrate = wins/(wins+loses)

        # print(f"{name_1} has code {code_1}")
        # print(f"{name_2} has code {code_2}")

        print(f"\n{name_1} wins against {name_2} {winrate * 100}% of times. Wins: {wins}, loses: {loses}, total games: {wins + loses}")

        return winrate

    def make_matrices(self):
        """Make 3 winrate matrices. One considering positions, one general and one considering one partner.
        """
        game_ids = self.match_data_df.gameId.unique()
        positions = ["TOP","JUNGLE", "MIDDLE", "BOTTOM", "UTILITY"]
        cols = ["gameId", "championId", "position", "win"]
        print("Making win against/with matrices")
        for game_id in game_ids:
            current_game = self.match_data_df[self.match_data_df['gameId'] == game_id][cols]

            current_game["championId"] = current_game["championId"].replace(self.champ_keys)

            # matrix respecting positions
            for pos in positions:
                matchup = current_game[current_game["position"] == pos]
                winner = int(matchup[matchup["win"] == True]["championId"])
                loser = int(matchup[matchup["win"] == False]["championId"])
                # print(matchup)
                self.win_against_matrix_pos[winner, loser] += 1

            # matrix without positions
            winners = list(current_game[current_game["win"] == True]["championId"])
            losers = list(current_game[current_game["win"] == False]["championId"])
            for winner in winners:
                for loser in losers:
                    self.win_against_matrix[winner, loser] += 1

            # break
        print("Saving files.")
        print(f"path: {self.win_against_matrix_pos_path}")
        print(f"path: {self.win_against_matrix_path}")
        np.savetxt(self.win_against_matrix_pos_path, self.win_against_matrix_pos, delimiter=",")
        np.savetxt(self.win_against_matrix_path, self.win_against_matrix, delimiter=",")

    def plot_winrate_matrices(self):
        """Use imgshow to plot winrates.
        """
        plt.imshow(self.win_against_matrix_pos)
        plt.title("Win Against Matrix Pos")
        plt.show()

        plt.imshow(self.win_against_matrix)
        plt.title("Win Against Matrix")
        plt.show()

    def get_matrices(self):
        """Load matrices from files or recalculate if the files are missing.
        """
        print("Loading matrices".center(30, "="))
        if os.path.isfile(self.win_against_matrix_path) and os.path.isfile(self.win_against_matrix_pos_path):
            print("\tLoading matrix from csv.")
            self.win_against_matrix = np.genfromtxt(self.win_against_matrix_path, delimiter=",")
            self.win_against_matrix_pos = np.genfromtxt(self.win_against_matrix_pos_path, delimiter=",")
        else:
            print("\tCan not load from csv. Making new matrices.")
            self.make_matrices()

    def create_selected_cols_game_df(self, game_df):
        """Select important columns from the game data

        Args:
            game_df (pd.DataFrame): dataframe containing one game
        """
        picks = game_df.loc[:,"championId"]
        positions = game_df.loc[:, "position"]
        ban_cols = [f"ban{x}" for x in range(10)]
        pick_cols = [f"pick{x}" for x in range(10)]
        pos_cols = [f"pos{x}" for x in range(5)]
        bans = game_df[ban_cols].iloc[0, :]

        cur_game_df = pd.DataFrame(picks.append(positions[0:5]).append(bans)).transpose()
        colnames = [pick_cols, pos_cols, ban_cols]
        colnames = [y for x in colnames for y in x]
        cur_game_df.columns = colnames

        self.game_selected_cols = pd.concat([self.game_selected_cols, cur_game_df], axis=0)

    def sub_lists(self, list_in):
        """Find all sublists of a list without empty and full

        Args:
            list_in (list): list of player ids [0,1, ..., 9]

        Returns:
            list: a list of all possible sublists
        """
        sublists = []
        for i in range(len(list_in) + 1):
            for j in range(i+1, len(list_in) + 1):
                sub = list_in[i:j]
                if len(sub) == len(list_in):
                    continue
                sublists.append(sub)
        return sublists

    def one_hot_next_pick(self, pick_id):
        """One hot encode next pick

        Args:
            pick_id (int): who is picking next

        Returns:
            list: encoded input
        """
        out = [0 for _ in range(10)]
        out[pick_id] = 1
        return out

    def one_hot_position(self):
        """One hot encode position for each player using pd.get_dummies
        """
        pos_cols = [f"pos{x}" for x in range(5)]
        categories = ["TOP","MIDDLE","UTILITY","JUNGLE","BOTTOM"]
        for pos in pos_cols:
            self.final_df = pd.concat([self.final_df, pd.get_dummies(self.final_df[pos], prefix=pos,dummy_na=False)],axis=1).drop([pos],axis=1)

        # print(self.final_df)

        # reorder cols
        cols = self.final_df.columns.tolist()
        # put answer at the end
        cols.pop(30)
        cols.append("answer")
        self.final_df = self.final_df[cols]
        print("Saving final df to csv.")
        print(f"path: {self.final_df_path}")
        self.final_df.to_csv(self.final_df_path, index=False)


    def remove_picks(self):
        # drop_lists = self.sub_lists(self.player_ids)
        # print(self.drop_lists)
        # new_rows = pd.DataFrame()
        colnames = []
        pick_cols = [f"pick{x}" for x in range(10)]
        pos_cols = [f"pos{x}" for x in range(5)]
        ban_cols = [f"ban{x}" for x in range(10)]
        next_hot = [f"next_hot{x}" for x in range(10)]
        colnames.extend(pick_cols)
        colnames.extend(pos_cols)
        colnames.extend(ban_cols)
        colnames.extend(next_hot)
        colnames.append("answer")
        # print(colnames)
        new_rows = []
        for index, row in self.game_selected_cols.iterrows():
            # print(row["pick0"])
            for drop in self.drop_lists:
                drop_cols = [f"pick{x}" for x in drop]

                # print(new_row)
                for pick_next in drop:
                    new_row = row.copy()
                    new_row[drop_cols] = 0
                    new_row = new_row.tolist()

                    pick_next_hot = self.one_hot_next_pick(pick_next)
                    answer = row[f"pick{pick_next}"]
                    # print(answer)
                    pick_next_hot.append(answer)
                    new_row.extend(pick_next_hot)
                    # print(new_row)
                    # new_rows = pd.concat([new_rows, pd.DataFrame(new_row).transpose()], axis = 0)
                    new_rows.append(new_row)

        # for row in new_rows:
        #     print(row)
        #     break
        self.final_df = pd.DataFrame(data = new_rows, columns=colnames)
        # print(new_rows)
        print("Saving final df to csv.")
        print(f"path: {self.final_df_path}")
        self.final_df.to_csv(self.final_df_path, index=False)

    def augment_one_game(self, game_df):
        """Create a lot of training data from one game. Assume all high elo team comps are correct.

        Args:
            game_df (pd.DataFrame): DataFrame containing information about 1 game (10 rows)
        """
        # print(f"\tProcessing game: {game_df.iloc[0, 0]}")
        self.create_selected_cols_game_df(game_df)


    def validate_positions(self, game_df):
        """Check whether there are max of 2 of the same position in a game

        Args:
            game_df (pd.DataFrame): dataframe containing only one game (selected using game id)

        Returns:
            bool: True if the positions are valid, False otherwise
        """
        pos_counts = game_df.groupby("position")["position"].count()
        for i in range(len(pos_counts)):
            if pos_counts[i] > 2:
                print("Invalid pos.")
                return False
        return True

    def process_data(self):
        """Main function, Data pipeline
        """
        print("Processing data".center(30, "="))

        game_ids = self.match_data_df["gameId"].unique()
        # for dropping picks
        self.drop_lists = self.sub_lists(self.player_ids)

        for game_id in game_ids:
            self.get_positions_using_role_ident(self.match_data_df[self.match_data_df["gameId"] == game_id])
            if self.validate_positions(self.match_data_df[self.match_data_df["gameId"] == game_id]):
                self.augment_one_game(self.match_data_df[self.match_data_df["gameId"] == game_id])
        print("Saving to file.")
        print(f"path: {self.augmented_path}")
        self.game_selected_cols.to_csv(self.augmented_path, index=False)

        self.remove_picks()
        self.one_hot_position()

    def get_position_from_role_and_lane(self, row):
        """Use lane and role to get the position in game

        Args:
            row ([type]): row of a pandas dataframe

        Returns:
            str: calculated position
        """
        role = row["role"]
        lane = row["lane"]
        # print(f"Role: {role}\t\tLane: {lane}" )
        position_map = {
            ("MIDDLE", "SOLO"): "MID",
            ("TOP", "SOLO"): "TOP",
            ("JUNGLE", "NONE"): "JNG",
            ("BOTTOM", "DUO_CARRY"): "BOT",
            ("BOTTOM", "DUO_SUPPORT"): "SUP",
            ("NONE", "DUO_SUPPORT"): "SUP",
            ("NONE", "DUO"): "BOT",
            ('MIDDLE', 'DUO_SUPPORT'): "SUP",
            ('MIDDLE', 'DUO_CARRY'): "BOT",
            ('BOTTOM', 'SOLO'): "BOT",
            ('BOTTOM', 'DUO'): "BOT",
            ('TOP', 'DUO'): "TOP",
            ('MIDDLE', 'DUO'): "MID",
            ('TOP', 'DUO_SUPPORT'): "SUP"
        }
        # print(position_map[(lane, role)])
        return position_map[(lane, role)]

    def get_positions_using_role_ident(self, game_df):
        """Get champion positions using https://github.com/meraki-analytics/role-identification

        Args:
            game_df (pd.DataFrame): dataframe containing only one game (selected using game id)

        Returns:
            list: positions of the champions from in the same order as the input df
        """
        # get champions by team
        team1 = game_df.championId[0:5].tolist()
        team2 = game_df.championId[5:10].tolist()
        # get positions of champions
        positions1 = get_roles(self.pos_data, team1)
        positions2 = get_roles(self.pos_data, team2)
        # swap keys with values
        positions1 = dict((v,k) for k,v in positions1.items())
        positions2 = dict((v,k) for k,v in positions2.items())

        final_pos = []
        for champ in team1:
            final_pos.append(positions1[champ])
        for champ in team2:
            final_pos.append(positions2[champ])

        return final_pos

    def make_position_col(self):
        """Add a position column to the dataset
        """
        game_ids = self.match_data_df["gameId"].unique()
        pos_list = []
        for game_id in game_ids:
            pos_list.extend(self.get_positions_using_role_ident(self.match_data_df[self.match_data_df["gameId"] == game_id]))
        # pos_list = [item for sublist in pos_list for item in sublist]
        # print(pos_list)
        print(f"Dims match data df: {self.match_data_df.shape}")
        print(f"Pos list len:{len(pos_list)}")
        print(f"Number of unique game ids: {len(game_ids)}, x10: {len(game_ids)*10}")
        self.match_data_df["position"] = pos_list
        
        # self.match_data_df["position"] = self.match_data_df.apply(self.get_position_from_role_and_lane, axis = 1)
        print("Saving match data df file.")
        print(f"path: {self.match_data_df_path}")
        self.match_data_df.to_csv(self.match_data_df_path, index=False)

    def get_winrate_using_my_codes_return_names(self, code_1, code_2, matrix, verbose = False):
        """Get 2 champion codes (my) and return winrate c1 vs c2

        Args:
            code_1 (int): Champion 1 code
            code_2 (int): Champion 2 code
            matrix (np array): Winrate matrix
            verbose (bool, optional): Print outputs. Defaults to False.

        Returns:
            float: Winrate between 0 and 1
        """
        wins = int(matrix[code_1][code_2])
        loses = int(matrix[code_2][code_1])

        name_1 = self.champion_translation_dict[code_1][0]
        name_2 = self.champion_translation_dict[code_2][0]

        try:
            winrate = wins/(wins+loses)
        except:
            winrate = 0.0
        if verbose:
            print(f"{name_1} wins with {name_2} {winrate*100}% of times.")

        return (name_1, name_2, round(winrate, 4), wins+loses)

    def winrate_matrices_to_json(self):
        """Convert winrate matrices to sorted JSON dicts for JS use
        """
        self.win_against_matrix
        self.win_against_matrix_pos
        win_agains_dict = {}
        win_agains_dict_sorted = {}
        for row_id in range(self.NUMBER_OF_CHAMPIONS):
            for col_id in range(self.NUMBER_OF_CHAMPIONS):
                name_tuple = self.get_winrate_using_my_codes_return_names(row_id, col_id, self.win_against_matrix_pos, verbose=False)
                if name_tuple[0] == name_tuple[1]:
                    continue
                if name_tuple[3] == 0:
                    continue
                if name_tuple[0] in win_agains_dict:
                    win_agains_dict[name_tuple[0]].append({"id": name_tuple[1], "winrate": name_tuple[2], "n": name_tuple[3]})
                else:
                    win_agains_dict[name_tuple[0]] = [(name_tuple[1], name_tuple[2], name_tuple[3])]
                    win_agains_dict[name_tuple[0]] = [{"id": name_tuple[1], "winrate": name_tuple[2], "n": name_tuple[3]}]
            # sort by winrate
            try:
                win_agains_dict[name_tuple[0]].sort(key = lambda x: -(x["winrate"]*x["n"]))
            except:
                pass
        
        # print(win_agains_dict["Ahri"])

        with open(self.win_against_pos_json_path, 'w') as json_file:
            json.dump(win_agains_dict, json_file)

        with open(self.win_against_pos_json_path_public, 'w') as json_file:
            json.dump(win_agains_dict, json_file) 
    
    def augment(self, bool_plot = False):
        self.make_position_col()
        # self.process_data()
        self.get_matrices()
        if bool_plot:
            self.plot_winrate_matrices()
        # self.populate_champ_pos_dict()
        self.winrate_matrices_to_json()

if __name__ == "__main__":
    API_LIMIT_1s = 20
    API_LIMIT_2m = 100
    SLEEP_TIME = 120/(API_LIMIT_2m - 1)

    # get api key
    keys_path = os.path.dirname(os.path.abspath(__file__))+"/../../keys.json"
    with open(keys_path) as f:
        keys = json.load(f)

    # get game version
    x = requests.get('https://ddragon.leagueoflegends.com/api/versions.json')
    GAME_VERSION = x.json()[0]
    GAME_VERSION_SHORT = GAME_VERSION[0:5]

    RIOT_API_KEY = keys["riot_api"]
    lol_watcher = LolWatcher(RIOT_API_KEY)
    dataLoader = DataLoader(lol_watcher=lol_watcher, GAME_VERSION=GAME_VERSION_SHORT, SLEEP_TIME=SLEEP_TIME, REGIONS=["eun1"], force_overwrite=False)
    # dataLoader.get_players_df_from_api()
    dataLoader.load_data()

    dataAugmenter = DataAugmenter(dataLoader.match_data_df, dataLoader.match_data_df_path)
    dataAugmenter.augment(bool_plot=False)
    dataAugmenter.get_winrate_using_names("Ezreal", "Aphelios")

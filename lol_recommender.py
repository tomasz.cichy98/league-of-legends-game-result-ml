import sys
import os
import argparse
import json
import requests
from riotwatcher import LolWatcher, ApiError
import random
import math
import pandas as pd

import threading
import multiprocessing

from src.data.data_loader import DataLoader
from src.data.data_augmenter import DataAugmenter
# from src.models.train_model import TrainerTester
from src.models.win_rate_recommender import WinRateRecommender

parser = argparse.ArgumentParser(
    description='Download the data and retrain the model.')
parser.add_argument("-FD", "--force_redownload",
                    help="redownload all data", action="store_true")
parser.add_argument("-PM", "--plot_matrix",
                    help="plot winrate matrices", action="store_true")


args = parser.parse_args()

print(f"force_download set to: {args.force_redownload}")
print(f"plot_matrix set to: {args.plot_matrix}")

REGIONS = ['eun1', "br1", "euw1", "jp1", "kr",
           "la1", "la2", "na1", "oc1", "tr1", "ru"]

# init watcher
API_LIMIT_1s = 20
API_LIMIT_2m = 100
SLEEP_TIME = 120/(API_LIMIT_2m - 1)

# get api key
keys_path = os.path.dirname(os.path.abspath(__file__))+"/keys.json"
with open(keys_path) as f:
    keys = json.load(f)

# get game version
x = requests.get('https://ddragon.leagueoflegends.com/api/versions.json')
GAME_VERSION = x.json()[0]
GAME_VERSION_SHORT = GAME_VERSION[0:5]

RIOT_API_KEY = keys["riot_api"]
lol_watcher = LolWatcher(RIOT_API_KEY)

# loader
NUM_WORKERS = multiprocessing.cpu_count()
# make parallel workers
workers = []
for reg in REGIONS:
    worker = DataLoader(lol_watcher=lol_watcher, GAME_VERSION=GAME_VERSION_SHORT, SLEEP_TIME=SLEEP_TIME, REGIONS=[reg],
                        force_overwrite=args.force_redownload, selected_tiers=["BRONZE", "SILVER", "GOLD", "PLATINUM"])
    workers.append(worker)

worker_threads = []
for worker in workers:
    def worker_fn(): return worker.load_data()
    t = threading.Thread(target=worker_fn)
    t.start()
    worker_threads.append(t)

# wait for end
for t in worker_threads:
    t.join()
print("Done parallel total.")

# combine data
match_data_df = pd.DataFrame()
for reg in REGIONS:
    match_data_df_path = f"{os.path.dirname(os.path.abspath(__file__))}/data/external/match_data_tiers_df_{GAME_VERSION_SHORT}_{reg}.csv"
    df = pd.read_csv(match_data_df_path)
    match_data_df = pd.concat([match_data_df, df])

# augmenter
combined_path = f"{os.path.dirname(os.path.abspath(__file__))}/data/external/match_data_tiers_df_{GAME_VERSION_SHORT}_big.csv"
dataAugmenter = DataAugmenter(
    match_data_df, combined_path)
dataAugmenter.augment(args.plot_matrix)

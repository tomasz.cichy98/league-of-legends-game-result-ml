# Make a new icon dataset
# Train model

from src.data.champion_icons_loader import ChampionIconLoader

print("Making the icons dataset.")
cil = ChampionIconLoader()
cil.load_data()

print("Training the model.")
import src.models.train_icon_model

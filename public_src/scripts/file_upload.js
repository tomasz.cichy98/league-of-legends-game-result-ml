// to minify use https://obfuscator.io/ with settings 
// compact, mangled, simplify, dead code injection, disable console, self defending
// string array

// const ICON_SIZE = 120;
const IMG_WIDTH = 1280;
const IMG_HEIGHT = 720;
const BAN_ICON_SIZE = 28;
const ICON_SIZE = 28;
const PICK_ICON_SIZE = 60;
const PICK_ICON_BOX_PADDING = 5;
const BAN_ICON_BOX_PADDING = 2;
const GAME_VERSION = "10.16.1"

const sess = new onnx.InferenceSession()
const loadingModelPromise = sess.loadModel("./model.onnx").then(console.log("Loaded model."));

var dropRegion = document.getElementById("drop-region")
var imagePreviewRegion = document.getElementById("image-preview")
var canvasPrev = document.getElementById('canvas-prev')
var ctxPrev = canvasPrev.getContext('2d')

var dummyCanvasPicks = document.getElementById("dummy-canvas-picks")
var dummyCtxPicks = dummyCanvasPicks.getContext('2d')
var dummyCanvasBans = document.getElementById("dummy-canvas-bans");
var dummyCtxBans = dummyCanvasBans.getContext("2d")

var testCanvas = document.getElementById("test-canvas");
var recommendation_data
// onclick button
$("#run-example-button").on("click", runExample)
$("#drop-region").on("paste", handlePaste)
// set focus to the drop area

var champData
$.getJSON(`https://ddragon.leagueoflegends.com/cdn/${GAME_VERSION}/data/en_US/champion.json`, function (response) {
    champData = response.data
})

var predictCount = 0

var champ_boxes = {
    "bans": [
        // topright x, topright y
        { "x": 16, "y": 31 },
        { "x": 56, "y": 31 },
        { "x": 96, "y": 31 },
        { "x": 136, "y": 31 },
        { "x": 176, "y": 31 },
        { "x": 1077, "y": 31 },
        { "x": 1117, "y": 31 },
        { "x": 1157, "y": 31 },
        { "x": 1197, "y": 31 },
        { "x": 1237, "y": 31 },
    ],
    "ally": [
        { "x": 56, "y": 104 },
        { "x": 56, "y": 184 },
        { "x": 56, "y": 264 },
        { "x": 56, "y": 344 },
        { "x": 56, "y": 424 },
    ],
    "enemy": [
        { "x": 1194, "y": 104 },
        { "x": 1194, "y": 184 },
        { "x": 1194, "y": 264 },
        { "x": 1194, "y": 344 },
        { "x": 1194, "y": 424 }
    ]
}

var state = {
    "preds": {
        "bans": [],
        "ally": [],
        "enemy": [],
    }
}


$(document).ready(function () {
    // populateOutputTemplate(state);
    $.getJSON("./win_against_pos.json", function (data) {
        console.log("Loaded matchup data.");
        recommendation_data = data
        console.log(recommendation_data);
    }).fail(function () {
        console.log("An error has occurred.");
    });
    $("#drop-region").click();
})

function populateOutputTemplate(state_in) {
    let banOutTemplate = $("#ban-out-template").html();
    let allyOutTemplate = $("#ally-out-template").html();
    let enemyOutTemplate = $("#enemy-out-template").html();
    let allyMatchupListTemplate = $("#ally-matchup-template").html();
    let enemyMatchupListTemplate = $("#enemy-matchup-template").html();
    let modalTemplate = $("#champion-card-modal").html()
    let src = `https://ddragon.leagueoflegends.com/cdn/${GAME_VERSION}/img/champion/`

    // let outputParent = $("#output-parent")
    let banOutList = $("#bans-out")
    let allyOutputList = $("#ally-picks-out")
    let enemyOutputList = $("#enemy-picks-out")
    let modalsParent = $("#modals-parent")

    // clear the lists
    banOutList.empty()
    allyOutputList.empty()
    enemyOutputList.empty()
    modalsParent.empty()

    // state = JSON.parse(state)
    bans = state_in["preds"]["bans"]
    allyPicks = state_in["preds"]["ally"]
    enemyPicks = state_in["preds"]["enemy"]
    // console.log(bans);
    // outputParent.append(Mustache.render(outputTemplate))

    // bans
    $.each(bans, function (i, ban) {
        ban_data = champData[ban["id"]]
        banOutList.append(Mustache.render(banOutTemplate, ban_data));
    });
    // ally enemy
    $.each(allyPicks, function (i, pick) {
        pick = getPickData(pick)
        populateTeam(allyOutputList, pick, src, allyOutTemplate, allyMatchupListTemplate)
        makeModal(modalsParent, modalTemplate, pick)
    });
    $.each(enemyPicks, function (i, pick) {
        pick = getPickData(pick)
        populateTeam(enemyOutputList, pick, src, enemyOutTemplate, enemyMatchupListTemplate)
        makeModal(modalsParent, modalTemplate, pick)
    });
}

function getPickData(pick) {
    if (pick["id"] != "None") {
        pick_out = champData[pick["id"]]
        pick_out["matchups"] = pick["matchups"]
    } else {
        pick_out = pick
    }

    return pick_out
}

function makeModal(parent, template, data) {
    // if (data["name"] == "LeBlanc"){
    //     data["name"] = "Leblanc"
    // }
    if (data["id"] == "None") {
        return 0
    }
    modal_data_link = `https://ddragon.leagueoflegends.com/cdn/${GAME_VERSION}/data/en_US/champion/${data["id"]}.json`
    let data_obj
    $.getJSON(modal_data_link, function (dataJSON) {
        data_obj = dataJSON.data[data["id"]]
        data_obj["version"] = data["version"]
        parent.append(Mustache.render(template, data_obj))
    })
    // console.log(data_obj["id"]);
}

function populateTeam(outList, pick, src, template, matchupTemplate) {
    pick["src"] = src + pick["id"] + ".png"
    if (pick["id"] == "None") {
        pick = { "name": "None", "id": "None", "src": "./images/None.png", "show_card":false }
    }
    outList.append(Mustache.render(template, pick))
    if (pick["id"] != "None") {
        matchupList = $(`#${pick.id}-matchup`)
        populateMatchups(pick, matchupList, matchupTemplate)
    }
}

function populateMatchups(pick, matchupList, template) {
    $.each(pick["matchups"], function (j, matchup) {
        matchupList.append(Mustache.render(template, matchup))
        if (j >= 2) {
            return false;
        }
    })
}

// read the json file
// var trans_dict
// $.getJSON("../translation_dict.json", function(json){
//     trans_dict = json
//     trans_dict = JSON.parse(json)
// })
// json request problem
var trans_dict = "[\"Aatrox\", \"Ahri\", \"Akali\", \"Alistar\", \"Amumu\", \"Anivia\", \"Annie\", \"Aphelios\", \"Ashe\", \"AurelionSol\", \"Azir\", \"Bard\", \"Blitzcrank\", \"Brand\", \"Braum\", \"Caitlyn\", \"Camille\", \"Cassiopeia\", \"Chogath\", \"Corki\", \"Darius\", \"Diana\", \"DrMundo\", \"Draven\", \"Ekko\", \"Elise\", \"Evelynn\", \"Ezreal\", \"Fiddlesticks\", \"Fiora\", \"Fizz\", \"Galio\", \"Gangplank\", \"Garen\", \"Gnar\", \"Gragas\", \"Graves\", \"Hecarim\", \"Heimerdinger\", \"Illaoi\", \"Irelia\", \"Ivern\", \"Janna\", \"JarvanIV\", \"Jax\", \"Jayce\", \"Jhin\", \"Jinx\", \"Kaisa\", \"Kalista\", \"Karma\", \"Karthus\", \"Kassadin\", \"Katarina\", \"Kayle\", \"Kayn\", \"Kennen\", \"Khazix\", \"Kindred\", \"Kled\", \"KogMaw\", \"Leblanc\", \"LeeSin\", \"Leona\", \"Lillia\", \"Lissandra\", \"Lucian\", \"Lulu\", \"Lux\", \"Malphite\", \"Malzahar\", \"Maokai\", \"MasterYi\", \"MissFortune\", \"MonkeyKing\", \"Mordekaiser\", \"Morgana\", \"Nami\", \"Nasus\", \"Nautilus\", \"Neeko\", \"Nidalee\", \"Nocturne\", \"None\", \"Nunu\", \"Olaf\", \"Orianna\", \"Ornn\", \"Pantheon\", \"Poppy\", \"Pyke\", \"Qiyana\", \"Quinn\", \"Rakan\", \"Rammus\", \"RekSai\", \"Renekton\", \"Rengar\", \"Riven\", \"Rumble\", \"Ryze\", \"Sejuani\", \"Senna\", \"Sett\", \"Shaco\", \"Shen\", \"Shyvana\", \"Singed\", \"Sion\", \"Sivir\", \"Skarner\", \"Sona\", \"Soraka\", \"Swain\", \"Sylas\", \"Syndra\", \"TahmKench\", \"Taliyah\", \"Talon\", \"Taric\", \"Teemo\", \"Thresh\", \"Tristana\", \"Trundle\", \"Tryndamere\", \"TwistedFate\", \"Twitch\", \"Udyr\", \"Urgot\", \"Varus\", \"Vayne\", \"Veigar\", \"Velkoz\", \"Vi\", \"Viktor\", \"Vladimir\", \"Volibear\", \"Warwick\", \"Xayah\", \"Xerath\", \"XinZhao\", \"Yasuo\", \"Yone\", \"Yorick\", \"Yuumi\", \"Zac\", \"Zed\", \"Ziggs\", \"Zilean\", \"Zoe\", \"Zyra\"]"
trans_dict = JSON.parse(trans_dict)

// open file selector when clicked on the drop region
var fakeInput = document.createElement("input");
fakeInput.type = "file";
fakeInput.accept = "image/*";
fakeInput.multiple = true;
// dropRegion.addEventListener('click', function () {
//     fakeInput.click();
// });

$("#upload-click").on("click", function () {
    fakeInput.click();
})

fakeInput.addEventListener("change", function () {
    var files = fakeInput.files;
    handleFiles(files);
});

function preventDefault(e) {
    e.preventDefault();
    e.stopPropagation();
}

var drag_counter = 0

dropRegion.addEventListener('dragenter', dragEnterAddClass, false)
dropRegion.addEventListener('dragleave', dragLeaveRemoveClass, false)
dropRegion.addEventListener('dragover', preventDefault, false)
dropRegion.addEventListener('drop', dropRemoveClass, false)
dropRegion.addEventListener('dragexit', preventDefault, false)

// dropRegion.addEventListener('drop', preventDefault, false)

function dropRemoveClass(e) {
    e.preventDefault();
    drag_counter = 0;
    dropRegion.classList.remove("is-dragover")
}

function dragEnterAddClass(e) {
    e.preventDefault();
    drag_counter++;
    // e.stopPropagation();
    dropRegion.classList.add("is-dragover")
}

function dragLeaveRemoveClass(e) {
    e.preventDefault();
    drag_counter--;
    // e.stopPropagation();
    if (drag_counter === 0) {
        dropRegion.classList.remove("is-dragover")
    }
}


function handleDrop(e) {
    var dt = e.dataTransfer,
        files = dt.files;
    if (files.length) {
        handleFiles(files);
    } else {
        // check for img
        var html = dt.getData('text/html'),
            match = html && /\bsrc="?([^"\s]+)"?\s*/.exec(html),
            url = match && match[1];

        if (url) {
            uploadImageFromURL(url);
            return;
        }
    }

    function uploadImageFromURL(url) {
        var img = new Image;
        var c = document.createElement("canvas");
        var ctx = c.getContext("2d");

        img.onload = function () {
            c.width = this.naturalWidth;     // update canvas size to match image
            c.height = this.naturalHeight;
            ctx.drawImage(this, 0, 0);       // draw in image
            c.toBlob(function (blob) {        // get content as PNG blob
                // call our main function
                handleFiles([blob]);
            }, "image/png");
        };
        img.onerror = function () {
            alert("Error in uploading");
        }
        img.crossOrigin = "";              // if from different origin
        img.src = url;
    }
}

dropRegion.addEventListener('drop', handleDrop, false);

function handleFiles(files) {
    for (var i = 0, len = files.length; i < len; i++) {
        if (validateImage(files[i]))
            previewAndUploadImage(files[i]);
    }
}

function validateImage(image) {
    // check the type
    var validTypes = ['image/jpeg', 'image/png', 'image/gif'];
    if (validTypes.indexOf(image.type) === -1) {
        alert("Invalid File Type");
        return false;
    }

    // check the size
    var maxSizeInBytes = 10e6; // 10MB
    if (image.size > maxSizeInBytes) {
        alert("File too large");
        return false;
    }
    return true;
}

function handlePaste() {
    console.log("Pasted");
    var item = event.clipboardData.items[0];
    console.log(item);

    if (item.type.indexOf("image") === 0) {
        var blob = item.getAsFile();

        previewAndUploadImage(blob)
    }
}

function previewAndUploadImage(image) {
    // remove previous img
    var prev = document.getElementsByClassName('image-view')
    while (prev[0]) {
        prev[0].parentNode.removeChild(prev[0]);
    }
    // container
    var imgView = document.createElement("div");
    imgView.className = "image-view";
    imagePreviewRegion.appendChild(imgView);

    // previewing image
    var img = document.createElement("img");
    img.id = "img-preview"
    imgView.appendChild(img);

    // progress overlay
    // var overlay = document.createElement("div");
    // overlay.className = "overlay";
    // imgView.appendChild(overlay);

    // read the image...
    var reader = new FileReader();
    reader.onload = function (e) {
        img.src = e.target.result;
    }
    reader.readAsDataURL(image);

    img.onload = function () {
        // console.log("loaded an image");
        // resize to 1280x720

        var imgPrevEl = document.getElementById('img-preview')
        ctxPrev.clearRect(0, 0, IMG_WIDTH, IMG_HEIGHT)
        ctxPrev.drawImage(imgPrevEl, 0, 0, IMG_WIDTH, IMG_HEIGHT)

        // let imageMarvin = new MarvinImage();
        // imageMarvin.load(canvasPrev.toDataURL(), function () {
        //     Marvin.scale(imageMarvin.clone(), imageMarvin, IMG_WIDTH, IMG_HEIGHT);
        //     imageMarvin.draw(canvasPrev)

        //     drawBBoxesOnInput()

        //     classifyImage()
        // })

        // console.log(imgPrevEl);
        drawBBoxesOnInput()
        classifyImage()
    }
}

function classifyImage() {
    // await loadingModelPromise;
    // classify
    let box_size = 0
    let pad = 0
    predictCount = 0
    $.each(champ_boxes, function (i, box_type) {
        $.each(box_type, function (j, box) {
            // console.log(i);
            if (i != "bans") {
                box_size = PICK_ICON_SIZE
                pad = PICK_ICON_BOX_PADDING
                dummyCanvas = dummyCanvasPicks
                dummyCtx = dummyCtxPicks
            } else {
                box_size = BAN_ICON_SIZE
                pad = BAN_ICON_BOX_PADDING
                dummyCanvas = dummyCanvasBans
                dummyCtx = dummyCtxBans
            }
            // var scale = (ICON_SIZE) / (box_size + 2 * pad)

            ctxPrev.rect(box["x"] - pad, box["y"] - pad, box_size + 2 * pad, box_size + 2 * pad)
            // get data from the bbox
            let imgData = ctxPrev.getImageData(box["x"] - pad, box["y"] - pad, box_size + 2 * pad, box_size + 2 * pad)

            // rescale to 28x28
            dummyCtx.putImageData(imgData, 0, 0)
            let imageMarvin = new MarvinImage();
            imageMarvin.load(dummyCanvas.toDataURL(), async function () {
                // scale
                Marvin.scale(imageMarvin.clone(), imageMarvin, ICON_SIZE, ICON_SIZE);
                // draw
                imageMarvin.draw(testCanvas)
                // get data from context
                imgData = testCanvas.getContext("2d").getImageData(0, 0, ICON_SIZE, ICON_SIZE)
                // predict
                pred = await classify_icon(imgData).then(predictCount++)
                // console.log(pred);
                // update state
                state["preds"][i][j] = pred
                // update html
                // TODO: FIX THIS ABOMINATION
                populateOutputTemplate(state)
                // syncCount ++;
            })
        })
        // console.log(box_type);
    })
}

function drawBBoxesOnInput() {
    // draw bboxes
    ctxPrev.beginPath();
    ctxPrev.strokeStyle = "#FF0000";
    let box_size = 0
    let pad = 0
    $.each(champ_boxes, function (i, box_type) {
        $.each(box_type, function (j, box) {
            // console.log(i);
            if (i != "bans") {
                box_size = PICK_ICON_SIZE
                pad = PICK_ICON_BOX_PADDING
            } else {
                box_size = BAN_ICON_SIZE
                pad = BAN_ICON_BOX_PADDING
            }
            ctxPrev.rect(box["x"] - pad, box["y"] - pad, box_size + 2 * pad, box_size + 2 * pad)
        })
        // console.log(box_type);
    })
    ctxPrev.stroke();
}

function sleep(ms) {
    console.log("Sleeping");
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function classify_icon(imgData) {
    // let sess = new onnx.InferenceSession()
    // await sess.loadModel('./model.onnx')
    // var imgData = ctxPrev.getImageData(0, 0, ICON_SIZE, ICON_SIZE);
    let imgDataPre = preprocess_img_data(imgData.data)
    let input = new onnx.Tensor(new Float32Array(imgDataPre), "float32")
    // console.log(input);

    await loadingModelPromise;
    let outputMap = await sess.run([input]);
    let outputTensor = outputMap.values().next().value;
    let predictions = outputTensor.data;
    let preds_arr = Array.from(predictions)
    let maxPred = Math.max(...preds_arr)
    let argMaxPred = argMax(preds_arr)

    // round max pred
    let maxPredRound = Math.round((maxPred + Number.EPSILON) * 10000) / 10000

    // console.log(`Upload Output tensor: ${preds_arr}`)
    // console.log(`Upload Output max tensor: ${maxPred}`)
    // console.log(`Upload Output argmax: ${argMaxPred}`)
    // console.log(`Predicted class: ${trans_dict[argMaxPred]}`)
    // outputChampEl.textContent = trans_dict[argMaxPred]
    // outputConfidence.textContent = maxPred

    // console.log(state);
    return {
        "id": trans_dict[argMaxPred], "prob": maxPredRound, "matchups": get_recommendation(trans_dict[argMaxPred])
    }
}

function get_recommendation(name) {

    recomm = recommendation_data[name]
    // console.log(recomm);
    return recomm
}

function preprocess_img_data(imgData) {
    var red = []
    var green = []
    var blue = []
    var out = [];
    var i;
    for (i = 0; i < imgData.length; i += 4) {
        red.push(imgData[i])
        green.push(imgData[i + 1])
        blue.push(imgData[i + 2])
    }
    // console.log(red);
    out = out.concat(red).concat(green).concat(blue)
    // console.log(len(out));
    // out = imgData
    return out
}

function argMax(array) {
    return array.map((x, i) => [x, i]).reduce((r, a) => (a[0] > r[0] ? a : r))[1];
}

async function test() {
    const sess = new onnx.InferenceSession()
    await sess.loadModel('./model.onnx')
    const input = new onnx.Tensor(new Float32Array(120 * 120 * 4), 'float32', [1 * 4 * 120 * 120])
    const outputMap = await sess.run([input])
    const outputTensor = outputMap.values().next().value
    const predictions = outputTensor.data
    const preds_arr = Array.from(predictions)
    const maxPred = Math.max(...preds_arr)
    const argMaxPred = argMax(preds_arr)

    console.log(`Test Output tensor: ${preds_arr}`)
    console.log(`Test Output max tensor: ${maxPred}`)
    console.log(`Test Output argmax: ${argMaxPred}`)
}
// test()

function runExample() {
    // remove previous img
    var prev = document.getElementsByClassName('image-view')
    while (prev[0]) {
        prev[0].parentNode.removeChild(prev[0]);
    }
    // container
    var imgView = document.createElement("div");
    imgView.className = "image-view";
    imagePreviewRegion.appendChild(imgView);

    // previewing image
    var img = document.createElement("img");
    img.id = "img-preview"
    imgView.appendChild(img);

    // progress overlay
    // var overlay = document.createElement("div");
    // overlay.className = "overlay";
    // imgView.appendChild(overlay);

    // read the image...
    var reader = new FileReader();
    reader.onload = function (e) {
        img.src = "./images/example.png";
    }
    img.src = "./images/example.png";
    // reader.readAsDataURL(image);

    img.onload = function () {
        // console.log("loaded an image");
        var imgPrevEl = document.getElementById('img-preview')
        // console.log(imgPrevEl);
        ctxPrev.clearRect(0, 0, IMG_WIDTH, IMG_HEIGHT)
        ctxPrev.drawImage(imgPrevEl, 0, 0, IMG_WIDTH, IMG_HEIGHT)
        drawBBoxesOnInput()

        classifyImage()
    }
}